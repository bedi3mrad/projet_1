Beginner_Tutorials
=====================
Présentation de l'activité:

Turtlesim a une vitesse par défaut

L'utilisateur est demandé de saisir une valeur de vitesse ,à souhait , en respctant les contraintes suivantes :

-Une vitesse maximale : (vitesse_max)  

-Une vitesse minimale : (vitesse_min) 

---> Vous trouvez les valeurs de deux parametres dans le fichier: /config/configuration.yaml 

Environnements de travail
=========================
OS : Ubuntu 14.04 Trusty
ROS: Indigo

Démarrage
============
1-configuration de l'environnement :
 
 a- Placer le package beginner_tutoriels dans le repertoire suivant : 
 
    VOTRE_WORKSPACE/src/
 
 b- faire les etapes suivantes :
  
    $ cd ~/VOTRE_WORKSPACE
    
    $ source /opt/ros/indigo/setup.bash
    
    $ source devel/setup.bash
    
    $ catkin_make 
  
2-Tester le projet :

 a-lancez les commandes suivantes dans deux differents terminals:
  
    $ roscore : démarrer le Master 

    $ roslaunch beginner_tutorials turtlevitesse.launch :faire appelle aux noeuds (turtlesim_node et clavier)
     

 

