Beginner_tutorials_3
=====================
Cette activité consiste à controler un turtlesim via une mannette de jeu (PS4)

Environnements de travail
=========================
OS : Ubuntu 14.04 Trusty
ROS: Indigo

Instalattion 
=========================
configuration de manette avec le système ROS :

 1-Commencez par installer le package: 
         $ sudo apt-get install ros-indigo-joy
 
 2-Connectez votre joystick à votre ordinateur. Voyons maintenant si Linux a reconnu votre joystick: 

         $ls /dev/input/
   *Vous verrez une liste de tous vos périphériques d'entrée similaires à
    ci-dessous:
     
         by-id event0 event2 event4 event6 event8 mouse0 mouse2 uinput
         by-path event1 event3 event5 event7 js0 souris mouse1

 3-Comme vous pouvez le voir ci-dessus, les dispositifs joystick sont appelés par jsX; dans ce cas, notre joystick est js0.
 
 4-Assurons-nous que le joystick fonctionne: 
       
         $sudo jstest/dev/input/jsX
   *Vous verrez la sortie du joystick sur l'écran. Déplacez le joystick pour voir
     les données changer :
     
         La version du pilote est 2.1.0.
         Joystick (Logitech Logitech Cordless RumblePad 2) a 6 axes (X, Y, Z, Rz,  
         Hat0X, Hat0Y)
         et 12 boutons (BtnX, BtnY, BtnZ, BtnTL, BtnTR, BtnTL2, BtnTR2, BtnSelect, 
         BtnStart, BtnMode, BtnThumbL, BtnThumbR).
         Test ... (interrompre pour quitter)
         Axes: 0: 0 1: 0 2: 0 3: 0 4: 0 5: 0 Boutons: 0: off 1: off 2: off 3: off 
         4: off 5: off 6: off 7: off 8: off 9 : éteint 10: éteint 11: éteint
            
 5- Rendons maintenant le joystick accessible pour le joystick ROS :
 
         $ sudo chmod a+ rw / dev / input / jsX

 6- Donnons d'abord au nœud joystick quel périphérique joystick utiliser 
    la valeur par défaut est js0 :
    
         $ roscore
         $ rosparam set joy_node / dev "/ dev / input / jsX"

 -----> Nous pouvons maintenant démarrer le nœud de manette


Démarrage
============

 1-IMPORTANT : Il faut tout d'abord copier les contenus de 2 dossier ( src/Manette.py et launch/turtlemanette.launch ) respectivement dans les destinations suivantes : beginner_tutorials/src et beginner_tutorials/launch
               
 2-N'OUBLIEZ PAS de Refaire le deux processus MAKE/BUILD !!!


 3-Enfin,executez les commandes suivantes :
         $ roscore
         $ roslaunch beginner_tutorials turtlemanette.launch
         $ rosrun    beginner_tutorials Manette.py
