Introduction
=====================
Cette activité est dans le cadre de mieux comprendre et pratiquer avec les nouveaux outils de systeme ROS qui on l'a deja vu dans les diffrents tutoriels  

Environnements de travail
=========================
OS : Ubuntu 14.04 Trusty
ROS: Indigo

Démarrage
============
1-Configuration de l'environnement :

 a-Executez les etapes suivantes :
 
    $ cd ~/VOTRE_WORKSPACE
  
    $ source /opt/ros/indigo/setup.bash
  
    $ source devel/setup.bash
  
    $ catkin_make 
  
  b-Testez le projet :
   
  Suivez les étapes dans "Readme" pour chaque activité
 

